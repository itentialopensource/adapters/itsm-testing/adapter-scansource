
## 0.2.0 [05-25-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!2

---

## 0.1.4 [03-14-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!1

---

## 0.1.3 [08-13-2020] & 0.1.2 [08-13-2020] & 0.1.1 [08-13-2020]

- Initial Commit

See commit ca04e01

---
