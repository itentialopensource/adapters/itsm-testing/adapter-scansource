/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-scansource',
      type: 'Scansource',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Scansource = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Scansource Adapter Test', () => {
  describe('Scansource Class Tests', () => {
    const a = new Scansource(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const addressCollectionKey = 'fakedata';
    const addressAddressId = 555;
    describe('#addressCopyAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressCopyAddress(addressCollectionKey, addressAddressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressCopyAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressSourceCollectionKey = 'fakedata';
    const addressDestinationCollectionKey = 'fakedata';
    describe('#addressMoveAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressMoveAddress(addressSourceCollectionKey, addressDestinationCollectionKey, addressAddressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressMoveAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressAddressValidateBodyParam = {
      Street1: 'string',
      Street2: 'string',
      City: 'string',
      StateProvince: 'string',
      PostalCode: 'string',
      CountryCode: 'string'
    };
    describe('#addressValidate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressValidate(addressAddressValidateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.State);
                assert.equal('string', data.response.Classification);
                assert.equal(true, Array.isArray(data.response.Attributes));
                assert.equal('object', typeof data.response.EffectiveAddress);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressAddressAddAddressBodyParam = {
      AddressType: 4,
      Name: 'string',
      Attention: 'string',
      Phone: 'string',
      Street1: 'string',
      Street2: 'string',
      City: 'string',
      StateProvince: 'string',
      PostalCode: 'string',
      CountryCode: 'string',
      ExtendedData: 'string'
    };
    describe('#addressAddAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressAddAddress(addressCollectionKey, addressAddressAddAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressAddAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressRegion = 555;
    describe('#addressGetAddressCountries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressGetAddressCountries(addressRegion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressGetAddressCountries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressExists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressExists(addressCollectionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressExists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressCountryCode = 'fakedata';
    describe('#addressGetAddressProvinceStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressGetAddressProvinceStates(addressCountryCode, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressGetAddressProvinceStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressSearch(addressCollectionKey, null, null, null, null, null, addressCountryCode, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressAddressUpdateAddressBodyParam = {
      Id: 9,
      AddressType: 1,
      Name: 'string',
      Attention: 'string',
      Phone: 'string',
      Street1: 'string',
      Street2: 'string',
      City: 'string',
      StateProvince: 'string',
      PostalCode: 'string',
      CountryCode: 'string',
      ExtendedData: 'string'
    };
    describe('#addressUpdateAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressUpdateAddress(addressCollectionKey, addressAddressUpdateAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressUpdateAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressGetAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressGetAddress(addressCollectionKey, addressAddressId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.Id);
                assert.equal('string', data.response.Created);
                assert.equal('string', data.response.Updated);
                assert.equal(3, data.response.AddressType);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Attention);
                assert.equal('string', data.response.Phone);
                assert.equal('string', data.response.Street1);
                assert.equal('string', data.response.Street2);
                assert.equal('string', data.response.City);
                assert.equal('string', data.response.StateProvince);
                assert.equal('string', data.response.PostalCode);
                assert.equal('string', data.response.CountryCode);
                assert.equal('string', data.response.ExtendedData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressGetAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartCartCollectionKey = 'fakedata';
    const cartCartId = 555;
    const cartCartSaveCheckoutInfoBodyParam = {
      PONumber: 'string',
      EndUserPO: 'string',
      RequestedDeliveryDate: 'string',
      ShipMethodServiceLevelCode: 'string',
      ServiceLevelDescription: 'string',
      ShippingAccountNumber: 'string',
      FreightForwarderNumber: 'string',
      ShipComplete: false,
      Memo: 'string',
      PayorId: 'string',
      ShippingAddress: {
        Name: 'string',
        Street1: 'string',
        Street2: 'string',
        City: 'string',
        State: 'string',
        PostalCode: 'string',
        Country: 'string'
      },
      Carrier: {
        CarrierAddress: {
          Name: 'string',
          Street1: 'string',
          Street2: 'string',
          City: 'string',
          State: 'string',
          PostalCode: 'string',
          Country: 'string'
        },
        Phone: 'string'
      },
      Created: 'string',
      Updated: 'string'
    };
    describe('#cartSaveCheckoutInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartSaveCheckoutInfo(cartCartCollectionKey, cartCartSaveCheckoutInfoBodyParam, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartSaveCheckoutInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartCopyCart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartCopyCart(cartCartCollectionKey, cartCartId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartCopyCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartCartAddItemBodyParam = {
      Item: 'string',
      Quantity: 7
    };
    describe('#cartAddItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartAddItem(cartCartCollectionKey, cartCartAddItemBodyParam, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartAddItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartSourceCollectionKey = 'fakedata';
    const cartDestinationCollectionKey = 'fakedata';
    describe('#cartMoveCart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartMoveCart(cartSourceCollectionKey, cartDestinationCollectionKey, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartMoveCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartCartAddCartBodyParam = {
      Name: 'string'
    };
    describe('#cartAddCart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartAddCart(cartCartCollectionKey, cartCartAddCartBodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartAddCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartGetCheckoutInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cartGetCheckoutInfo(cartCartCollectionKey, cartCartId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.PONumber);
                assert.equal('string', data.response.EndUserPO);
                assert.equal('string', data.response.RequestedDeliveryDate);
                assert.equal('string', data.response.ShipMethodServiceLevelCode);
                assert.equal('string', data.response.ServiceLevelDescription);
                assert.equal('string', data.response.ShippingAccountNumber);
                assert.equal('string', data.response.FreightForwarderNumber);
                assert.equal(true, data.response.ShipComplete);
                assert.equal('string', data.response.Memo);
                assert.equal('string', data.response.PayorId);
                assert.equal('object', typeof data.response.ShippingAddress);
                assert.equal('object', typeof data.response.Carrier);
                assert.equal('string', data.response.Created);
                assert.equal('string', data.response.Updated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartGetCheckoutInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartExists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartExists(cartCartCollectionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartExists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartHasCurrent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartHasCurrent(cartCartCollectionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartHasCurrent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartCartUpdateItemBodyParam = {
      Item: 'string',
      Quantity: 8
    };
    describe('#cartUpdateItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartUpdateItem(cartCartCollectionKey, cartCartId, cartCartUpdateItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartUpdateItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartGetCartList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cartGetCartList(cartCartCollectionKey, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.CurrentCartId);
                assert.equal(true, Array.isArray(data.response.Carts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartGetCartList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartSwitchCurrentCart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartSwitchCurrentCart(cartCartCollectionKey, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartSwitchCurrentCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartCartUpdateCartBodyParam = {
      Name: 'string'
    };
    describe('#cartUpdateCart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartUpdateCart(cartCartCollectionKey, cartCartUpdateCartBodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartUpdateCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartGetCart - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cartGetCart(cartCartCollectionKey, cartCartId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.Id);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ExtendedData);
                assert.equal(true, Array.isArray(data.response.CartItems));
                assert.equal('object', typeof data.response.CartCheckoutInfo);
                assert.equal('string', data.response.Created);
                assert.equal('string', data.response.Updated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartGetCart', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoDartDealId = 'fakedata';
    describe('#ciscoDartGetDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ciscoDartGetDetail(ciscoDartDealId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.DealId);
                assert.equal(true, Array.isArray(data.response.Lines));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoDart', 'ciscoDartGetDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ciscoDartCustomerNumber = 'fakedata';
    describe('#ciscoDartGetSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ciscoDartGetSummary(ciscoDartCustomerNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CiscoDart', 'ciscoDartGetSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customConfigCustomerNumber = 'fakedata';
    describe('#customConfigGetCustomConfigs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.customConfigGetCustomConfigs(customConfigCustomerNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomConfig', 'customConfigGetCustomConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerCustomerNumber = 'fakedata';
    const customerRegion = 'fakedata';
    describe('#customerGetCustCreditData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.customerGetCustCreditData(customerCustomerNumber, customerRegion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'customerGetCustCreditData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerBusinessUnit = 'fakedata';
    describe('#customerGetPayers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.customerGetPayers(customerCustomerNumber, customerBusinessUnit, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Payers));
                assert.equal(true, Array.isArray(data.response.Errors));
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'customerGetPayers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoiceCustomerNumber = 'fakedata';
    const invoiceInvoiceNumber = 'fakedata';
    describe('#invoiceGetInvoiceDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.invoiceGetInvoiceDetail(invoiceCustomerNumber, invoiceInvoiceNumber, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.InvoiceNumber);
                assert.equal('string', data.response.SalesOrderNumber);
                assert.equal('object', typeof data.response.BillingAddress);
                assert.equal('object', typeof data.response.ShippingAddress);
                assert.equal('string', data.response.PONumber);
                assert.equal('string', data.response.EndUserPO);
                assert.equal('string', data.response.ReferenceNumber);
                assert.equal('string', data.response.ShipDate);
                assert.equal('string', data.response.Cancelled);
                assert.equal('string', data.response.DocType);
                assert.equal('string', data.response.SalesRepCode);
                assert.equal('string', data.response.SalesRepName);
                assert.equal('string', data.response.SalesRepEmail);
                assert.equal(1, data.response.FreightAmount);
                assert.equal(1, data.response.TaxAmount);
                assert.equal(4, data.response.InsuranceAmount);
                assert.equal(10, data.response.Total);
                assert.equal('string', data.response.Currency);
                assert.equal('string', data.response.EnteredByEmail);
                assert.equal('string', data.response.ShippingOption);
                assert.equal('string', data.response.ShippingOptionDescription);
                assert.equal(true, Array.isArray(data.response.InvoiceLines));
                assert.equal(true, Array.isArray(data.response.TrackingNumbers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoice', 'invoiceGetInvoiceDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#invoiceGetInvoiceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.invoiceGetInvoiceList(invoiceCustomerNumber, null, null, null, invoiceInvoiceNumber, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoice', 'invoiceGetInvoiceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#invoiceGetPDF - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.invoiceGetPDF(invoiceCustomerNumber, invoiceInvoiceNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.InvoiceNumber);
                assert.equal('string', data.response.InvoiceData);
                assert.equal(true, Array.isArray(data.response.Errors));
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoice', 'invoiceGetPDF', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#invoiceGetInvoiceSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.invoiceGetInvoiceSummary(invoiceCustomerNumber, null, null, null, invoiceInvoiceNumber, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoice', 'invoiceGetInvoiceSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const partnerProfileDeveloperEmail = 'fakedata';
    describe('#partnerProfileGetPartnerProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partnerProfileGetPartnerProfile(null, partnerProfileDeveloperEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.Identity);
                assert.equal('string', data.response.Platform);
                assert.equal('string', data.response.ContactName);
                assert.equal('string', data.response.ContactEmail);
                assert.equal('string', data.response.ContactPhone);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ContactInfo);
                assert.equal('string', data.response.APIMDeveloperEmail);
                assert.equal(false, data.response.IsApplication);
                assert.equal(true, data.response.IsActive);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PartnerProfile', 'partnerProfileGetPartnerProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const partnerProfileCustomerNumber = 'fakedata';
    describe('#partnerProfileGetValidate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.partnerProfileGetValidate(partnerProfileDeveloperEmail, partnerProfileCustomerNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PartnerProfile', 'partnerProfileGetValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const productProductAvailabilityBodyParam = {
      CustomerNumber: 'string',
      Warehouses: [
        'string'
      ],
      ItemNumbers: [
        'string'
      ]
    };
    describe('#productAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.productAvailability(productProductAvailabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Product', 'productAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const productProductPricingBodyParam = {
      CustomerNumber: 'string',
      Lines: [
        {}
      ]
    };
    describe('#productPricing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.productPricing(productProductPricingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Product', 'productPricing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const productCustomerNumber = 'fakedata';
    const productItemNumber = 'fakedata';
    const productPartNumberType = 555;
    describe('#productGetDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.productGetDetail(productCustomerNumber, productItemNumber, productPartNumberType, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ScanSourceItemNumber);
                assert.equal('string', data.response.ManufacturerItemNumber);
                assert.equal('string', data.response.Manufacturer);
                assert.equal('string', data.response.ItemStatus);
                assert.equal('string', data.response.PlantMaterialStatusValidfrom);
                assert.equal('string', data.response.ItemImage);
                assert.equal(true, data.response.ReboxItem);
                assert.equal(false, data.response.BStockItem);
                assert.equal('string', data.response.CatalogName);
                assert.equal('string', data.response.BusinessUnit);
                assert.equal('string', data.response.CategoryPath);
                assert.equal('string', data.response.ProductFamilyImage);
                assert.equal('string', data.response.ProductFamilyDescription);
                assert.equal('string', data.response.ProductFamilyHeadline);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ProductFamily);
                assert.equal('string', data.response.BaseUnitofMeasure);
                assert.equal('string', data.response.GeneralItemCategoryGroup);
                assert.equal(10, data.response.GrossWeight);
                assert.equal('string', data.response.MaterialGroup);
                assert.equal('string', data.response.MaterialType);
                assert.equal('string', data.response.BatteryIndicator);
                assert.equal('string', data.response.RoHSComplianceIndicator);
                assert.equal('string', data.response.ManufacturerDivision);
                assert.equal('string', data.response.CommodityImportCodeNumber);
                assert.equal('string', data.response.CountryofOrigin);
                assert.equal('string', data.response.UNSPSC);
                assert.equal('string', data.response.DeliveringPlant);
                assert.equal('string', data.response.MaterialFreightGroup);
                assert.equal(8, data.response.MinimumOrderQuantity);
                assert.equal(true, data.response.SalespersonInterventionRequired);
                assert.equal(true, data.response.SellviaEDI);
                assert.equal('string', data.response.SellviaWeb);
                assert.equal('string', data.response.SerialNumberProfile);
                assert.equal(5, data.response.PackagedLength);
                assert.equal(8, data.response.PackagedWidth);
                assert.equal(9, data.response.PackagedHeight);
                assert.equal(true, Array.isArray(data.response.ProductMedia));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Product', 'productGetDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#productSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.productSearch(productCustomerNumber, productItemNumber, productPartNumberType, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Product', 'productSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rmaRmaCreateRMABodyParam = {
      CustomerNumber: 'string',
      PONumber: 'string',
      ContactName: 'string',
      ContactPhone: 'string',
      ContactEmail: 'string'
    };
    describe('#rmaCreateRMA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rmaCreateRMA(rmaRmaCreateRMABodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.RMARequestCreated);
                assert.equal('string', data.response.Message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rma', 'rmaCreateRMA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderSalesOrderCreateOrderBodyParam = {
      CustomerNumber: 'string',
      BusinessUnit: 10,
      ShipMethodServiceLevelCode: 'string',
      ShippingAddress: {},
      Lines: [
        {}
      ]
    };
    describe('#salesOrderCreateOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderCreateOrder(salesOrderSalesOrderCreateOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.OrderNumber);
                assert.equal(false, data.response.OrderCreated);
                assert.equal('string', data.response.ErrorMessage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderCreateOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderSalesOrderCreateOrderAsyncBodyParam = {
      CustomerNumber: 'string',
      BusinessUnit: 4,
      Lines: [
        {}
      ]
    };
    describe('#salesOrderCreateOrderAsync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.salesOrderCreateOrderAsync(salesOrderSalesOrderCreateOrderAsyncBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderCreateOrderAsync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderSalesOrderGetShipQuoteBodyParam = {
      CustomerNumber: 'string',
      BusinessUnit: 'string',
      ShipToCountryCode: 'string',
      Lines: [
        {}
      ]
    };
    describe('#salesOrderGetShipQuote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetShipQuote(salesOrderSalesOrderGetShipQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal(true, Array.isArray(data.response.ThirdPartyItems));
                assert.equal(2, data.response.Insurance);
                assert.equal('string', data.response.CurrencyCode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetShipQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderSalesOrderGetVRDBodyParam = {
      CustomerNumber: 'string',
      BusinessUnit: 'string',
      ItemNumbers: [
        'string'
      ]
    };
    describe('#salesOrderGetVRD - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetVRD(salesOrderSalesOrderGetVRDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.VRDItems));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetVRD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderCustomerNumber = 'fakedata';
    const salesOrderSalesOrderNumber = 'fakedata';
    describe('#salesOrderGetOrderDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetOrderDetail(salesOrderCustomerNumber, salesOrderSalesOrderNumber, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.SalesOrderNumber);
                assert.equal('object', typeof data.response.BillingAddress);
                assert.equal('object', typeof data.response.ShippingAddress);
                assert.equal('string', data.response.PONumber);
                assert.equal('string', data.response.EndUserPO);
                assert.equal('string', data.response.ReferenceNumber);
                assert.equal('string', data.response.DocType);
                assert.equal('string', data.response.Status);
                assert.equal('string', data.response.LastShipDate);
                assert.equal('string', data.response.Memo);
                assert.equal('string', data.response.SalesRepCode);
                assert.equal('string', data.response.SalesRepName);
                assert.equal('string', data.response.SalesRepEmail);
                assert.equal(7, data.response.InsuranceAmount);
                assert.equal(6, data.response.TaxAmount);
                assert.equal(1, data.response.Total);
                assert.equal('string', data.response.Currency);
                assert.equal('string', data.response.EnteredByEmail);
                assert.equal('string', data.response.ShippingOptionDescription);
                assert.equal(8, data.response.FreightAmount);
                assert.equal(true, Array.isArray(data.response.SalesOrderLines));
                assert.equal(true, Array.isArray(data.response.InvoiceNumbers));
                assert.equal(true, Array.isArray(data.response.TrackingNumbers));
                assert.equal(true, Array.isArray(data.response.Deliveries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetOrderDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#salesOrderGetOrderList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetOrderList(salesOrderCustomerNumber, null, null, salesOrderSalesOrderNumber, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetOrderList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#salesOrderGetSerialNumbers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetSerialNumbers(salesOrderCustomerNumber, null, null, salesOrderSalesOrderNumber, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetSerialNumbers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesorderSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSalesorderSummary(null, null, salesOrderSalesOrderNumber, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'getSalesorderSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#salesOrderGetOrderSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetOrderSummary(salesOrderCustomerNumber, null, null, salesOrderSalesOrderNumber, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetOrderSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#salesOrderGetTrackingNumbers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderGetTrackingNumbers(salesOrderCustomerNumber, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Orders));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderGetTrackingNumbers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spaSpaAddSPABodyParam = {
      VendorNumber: 'string',
      DealId: 'string',
      PECategory: 'string',
      SettleOpt: 'string',
      SalesOrg: 4,
      AgreementDesc: 'string',
      ValidFrom: 'string',
      ValidTo: 'string',
      EndUser: 'string',
      Resellers: [
        'string'
      ],
      Materials: [
        {
          Material: 'string',
          MaterialEntered: 'string',
          MaxQuantity: 5,
          Rate: 7,
          CalcType: 'string',
          SuggestedPrice: 7
        }
      ]
    };
    describe('#spaAddSPA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spaAddSPA(spaSpaAddSPABodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.AgreementCreated);
                assert.equal('string', data.response.AgreementNumber);
                assert.equal(true, Array.isArray(data.response.Errors));
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spa', 'spaAddSPA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spaSpaGetAgreementDetailBodyParam = {
      AgreementNumber: [
        'string'
      ]
    };
    describe('#spaGetAgreementDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spaGetAgreementDetail(spaSpaGetAgreementDetailBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spa', 'spaGetAgreementDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spaCustomerNumber = 'fakedata';
    describe('#spaGetAgreementSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spaGetAgreementSummary(spaCustomerNumber, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.CustomerNumber);
                assert.equal('string', data.response.SalesOrg);
                assert.equal('string', data.response.DealId);
                assert.equal('string', data.response.EndUser);
                assert.equal('string', data.response.ValidOn);
                assert.equal('string', data.response.MfgCode);
                assert.equal('string', data.response.ControlNumber);
                assert.equal('string', data.response.Description);
                assert.equal(true, Array.isArray(data.response.AgreementTypes));
                assert.equal(true, Array.isArray(data.response.Agreements));
                assert.equal(true, Array.isArray(data.response.Errors));
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spa', 'spaGetAgreementSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemCentralCustomerNumber = 'fakedata';
    describe('#systemCentralGetSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemCentralGetSummary(systemCentralCustomerNumber, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemCentral', 'systemCentralGetSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trackingCustomerNumber = 'fakedata';
    describe('#trackingGetTracking - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trackingGetTracking(trackingCustomerNumber, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.TrackingDetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tracking', 'trackingGetTracking', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trackingGetShipmentTracking - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trackingGetShipmentTracking(trackingCustomerNumber, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Details));
                assert.equal('object', typeof data.response.notification);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tracking', 'trackingGetShipmentTracking', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorOrderSubscriptionId = 'fakedata';
    describe('#vendorOrderGetVendorSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vendorOrderGetVendorSubscription(vendorOrderSubscriptionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorOrder', 'vendorOrderGetVendorSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorQuoteVendor = 555;
    describe('#vendorQuoteGetVendorQuoteList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vendorQuoteGetVendorQuoteList(vendorQuoteVendor, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.AgreementDate);
                assert.equal('string', data.response.AgreementDescription);
                assert.equal('string', data.response.AgreementId);
                assert.equal('object', typeof data.response.PartnerAddress);
                assert.equal('object', typeof data.response.EndCustomerAddress);
                assert.equal('string', data.response.BillingMethod);
                assert.equal('string', data.response.ContractEndDate);
                assert.equal('string', data.response.ContractStartDate);
                assert.equal('string', data.response.CountryCode);
                assert.equal('string', data.response.CreateDate);
                assert.equal('string', data.response.CurrencyCode);
                assert.equal('string', data.response.DealCode);
                assert.equal('string', data.response.DealDescription);
                assert.equal('string', data.response.DealTerms);
                assert.equal('string', data.response.DiscountCode);
                assert.equal('string', data.response.DiscountDescription);
                assert.equal('string', data.response.DiscountEndUser);
                assert.equal('string', data.response.DiscountExpirationDate);
                assert.equal('string', data.response.DiscountName);
                assert.equal('string', data.response.DiscountTerms);
                assert.equal(4, data.response.DiscountType);
                assert.equal('string', data.response.EndCustomerId);
                assert.equal('string', data.response.EndCustomerName);
                assert.equal('string', data.response.FeatureKey);
                assert.equal(5, data.response.FunctionalLocation);
                assert.equal('string', data.response.GAICode);
                assert.equal('string', data.response.GovermentContractNumber);
                assert.equal('string', data.response.GovernmentAgency);
                assert.equal('string', data.response.GSAPriceList);
                assert.equal('string', data.response.LastModified);
                assert.equal(true, Array.isArray(data.response.Lines));
                assert.equal(8, data.response.MonthlyBilling);
                assert.equal(true, Array.isArray(data.response.Notes));
                assert.equal('string', data.response.OpportunityId);
                assert.equal('string', data.response.ReferenceId);
                assert.equal('string', data.response.OrderingCompanyId);
                assert.equal('string', data.response.OrderingCompanyName);
                assert.equal('string', data.response.ParentQuoteNumber);
                assert.equal('string', data.response.PartnerId);
                assert.equal('string', data.response.PartnerName);
                assert.equal('string', data.response.ProgramCode);
                assert.equal('string', data.response.ProgramDealRegExpirationDate);
                assert.equal('string', data.response.ProgramDealRegOrderDate);
                assert.equal('string', data.response.ProgramDescription);
                assert.equal(10, data.response.ProgramExtendedDiscount);
                assert.equal('string', data.response.ProgramTerms);
                assert.equal('string', data.response.PromotionCode);
                assert.equal('string', data.response.PromotionTerms);
                assert.equal('string', data.response.PTNumber);
                assert.equal('string', data.response.QuoteExpDate);
                assert.equal('string', data.response.QuoteNumber);
                assert.equal('string', data.response.QuoteOwner);
                assert.equal('string', data.response.RegionCode);
                assert.equal('string', data.response.ResellerId);
                assert.equal('string', data.response.SellingPriceDescription);
                assert.equal('string', data.response.SellingPriceDiscountId);
                assert.equal('string', data.response.ServiceContractCoTermNumber);
                assert.equal('string', data.response.ServiceContractStartDate);
                assert.equal('string', data.response.ServiceQuoteNumber);
                assert.equal('string', data.response.ServicesContractContact);
                assert.equal('string', data.response.ShipToAddress);
                assert.equal('string', data.response.ShipToId);
                assert.equal('string', data.response.ShipToName);
                assert.equal('string', data.response.SiteId);
                assert.equal('string', data.response.SpecialBidCode);
                assert.equal('string', data.response.SpecialBidDescription);
                assert.equal('string', data.response.SpecialBidEndUser);
                assert.equal('string', data.response.SpecialBidExpirationDate);
                assert.equal('string', data.response.SpecialBidTerms);
                assert.equal('string', data.response.Status);
                assert.equal(2, data.response.TotalListPrice);
                assert.equal(8, data.response.TotalNetPrice);
                assert.equal(9, data.response.TotalPOPrice);
                assert.equal(4, data.response.TotalSellingPrice);
                assert.equal('string', data.response.ChargeType);
                assert.equal('string', data.response.InitialTerm);
                assert.equal('string', data.response.RemainingTerm);
                assert.equal('string', data.response.OldQuantity);
                assert.equal('string', data.response.VendorKey);
                assert.equal('string', data.response.ErrorMessage);
                assert.equal(false, data.response.IsInErrorState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorQuote', 'vendorQuoteGetVendorQuoteList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorQuoteQuoteNumber = 'fakedata';
    describe('#vendorQuoteGetVendorQuote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vendorQuoteGetVendorQuote(vendorQuoteVendor, vendorQuoteQuoteNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.AgreementDate);
                assert.equal('string', data.response.AgreementDescription);
                assert.equal('string', data.response.AgreementId);
                assert.equal('object', typeof data.response.PartnerAddress);
                assert.equal('object', typeof data.response.EndCustomerAddress);
                assert.equal('string', data.response.BillingMethod);
                assert.equal('string', data.response.ContractEndDate);
                assert.equal('string', data.response.ContractStartDate);
                assert.equal('string', data.response.CountryCode);
                assert.equal('string', data.response.CreateDate);
                assert.equal('string', data.response.CurrencyCode);
                assert.equal('string', data.response.DealCode);
                assert.equal('string', data.response.DealDescription);
                assert.equal('string', data.response.DealTerms);
                assert.equal('string', data.response.DiscountCode);
                assert.equal('string', data.response.DiscountDescription);
                assert.equal('string', data.response.DiscountEndUser);
                assert.equal('string', data.response.DiscountExpirationDate);
                assert.equal('string', data.response.DiscountName);
                assert.equal('string', data.response.DiscountTerms);
                assert.equal(5, data.response.DiscountType);
                assert.equal('string', data.response.EndCustomerId);
                assert.equal('string', data.response.EndCustomerName);
                assert.equal('string', data.response.FeatureKey);
                assert.equal(10, data.response.FunctionalLocation);
                assert.equal('string', data.response.GAICode);
                assert.equal('string', data.response.GovermentContractNumber);
                assert.equal('string', data.response.GovernmentAgency);
                assert.equal('string', data.response.GSAPriceList);
                assert.equal('string', data.response.LastModified);
                assert.equal(true, Array.isArray(data.response.Lines));
                assert.equal(10, data.response.MonthlyBilling);
                assert.equal(true, Array.isArray(data.response.Notes));
                assert.equal('string', data.response.OpportunityId);
                assert.equal('string', data.response.ReferenceId);
                assert.equal('string', data.response.OrderingCompanyId);
                assert.equal('string', data.response.OrderingCompanyName);
                assert.equal('string', data.response.ParentQuoteNumber);
                assert.equal('string', data.response.PartnerId);
                assert.equal('string', data.response.PartnerName);
                assert.equal('string', data.response.ProgramCode);
                assert.equal('string', data.response.ProgramDealRegExpirationDate);
                assert.equal('string', data.response.ProgramDealRegOrderDate);
                assert.equal('string', data.response.ProgramDescription);
                assert.equal(4, data.response.ProgramExtendedDiscount);
                assert.equal('string', data.response.ProgramTerms);
                assert.equal('string', data.response.PromotionCode);
                assert.equal('string', data.response.PromotionTerms);
                assert.equal('string', data.response.PTNumber);
                assert.equal('string', data.response.QuoteExpDate);
                assert.equal('string', data.response.QuoteNumber);
                assert.equal('string', data.response.QuoteOwner);
                assert.equal('string', data.response.RegionCode);
                assert.equal('string', data.response.ResellerId);
                assert.equal('string', data.response.SellingPriceDescription);
                assert.equal('string', data.response.SellingPriceDiscountId);
                assert.equal('string', data.response.ServiceContractCoTermNumber);
                assert.equal('string', data.response.ServiceContractStartDate);
                assert.equal('string', data.response.ServiceQuoteNumber);
                assert.equal('string', data.response.ServicesContractContact);
                assert.equal('string', data.response.ShipToAddress);
                assert.equal('string', data.response.ShipToId);
                assert.equal('string', data.response.ShipToName);
                assert.equal('string', data.response.SiteId);
                assert.equal('string', data.response.SpecialBidCode);
                assert.equal('string', data.response.SpecialBidDescription);
                assert.equal('string', data.response.SpecialBidEndUser);
                assert.equal('string', data.response.SpecialBidExpirationDate);
                assert.equal('string', data.response.SpecialBidTerms);
                assert.equal('string', data.response.Status);
                assert.equal(1, data.response.TotalListPrice);
                assert.equal(9, data.response.TotalNetPrice);
                assert.equal(8, data.response.TotalPOPrice);
                assert.equal(6, data.response.TotalSellingPrice);
                assert.equal('string', data.response.ChargeType);
                assert.equal('string', data.response.InitialTerm);
                assert.equal('string', data.response.RemainingTerm);
                assert.equal('string', data.response.OldQuantity);
                assert.equal('string', data.response.VendorKey);
                assert.equal('string', data.response.ErrorMessage);
                assert.equal(true, data.response.IsInErrorState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorQuote', 'vendorQuoteGetVendorQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorUsageVendor = 555;
    const vendorUsageBid = 555;
    const vendorUsageInvoiceNumber = 555;
    describe('#vendorUsageGetVendorUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vendorUsageGetVendorUsage(vendorUsageVendor, vendorUsageBid, vendorUsageInvoiceNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.InvoiceHeaders));
                assert.equal(true, Array.isArray(data.response.ConsolidatedUses));
                assert.equal(true, Array.isArray(data.response.Errors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorUsage', 'vendorUsageGetVendorUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webHookId = 'fakedata';
    const webHookWebHookSubscribeBodyParam = {
      ContentType: 'string',
      WebHookURL: 'string',
      Description: 'string',
      Active: true
    };
    describe('#webHookSubscribe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webHookSubscribe(webHookWebHookSubscribeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.BusinessUnit);
                assert.equal('string', data.response.ContentType);
                assert.equal('string', data.response.URL);
                assert.equal('string', data.response.Description);
                assert.equal(true, data.response.Active);
                assert.equal('string', data.response.Created);
                assert.equal('string', data.response.Updated);
              } else {
                runCommonAsserts(data, error);
              }
              webHookId = data.response.id;
              saveMockData('WebHook', 'webHookSubscribe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webHookGetHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webHookGetHooks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebHook', 'webHookGetHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressClear - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressClear(addressCollectionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressClear', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addressDelete(addressCollectionKey, addressAddressId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Address', 'addressDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartClear - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartClear(cartCartCollectionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartClear', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cartItemId = 555;
    describe('#cartDeleteItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartDeleteItem(cartCartCollectionKey, cartItemId, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartDeleteItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cartDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cartDelete(cartCartCollectionKey, cartCartId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cart', 'cartDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#salesOrderCancel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.salesOrderCancel(salesOrderCustomerNumber, salesOrderSalesOrderNumber, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'salesOrderCancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webHookDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webHookDelete(webHookId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-scansource-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebHook', 'webHookDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
