
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:52PM

See merge request itentialopensource/adapters/adapter-scansource!13

---

## 0.4.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-scansource!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:10PM

See merge request itentialopensource/adapters/adapter-scansource!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:25PM

See merge request itentialopensource/adapters/adapter-scansource!9

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:07PM

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:07PM

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:47PM

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:13AM

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!4

---

## 0.3.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!3

---

## 0.2.0 [05-25-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!2

---

## 0.1.4 [03-14-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-scansource!1

---

## 0.1.3 [08-13-2020] & 0.1.2 [08-13-2020] & 0.1.1 [08-13-2020]

- Initial Commit

See commit ca04e01

---
