## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Scansource. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Scansource.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for ScanSource. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">addressGetAddress(collectionKey, addressId, callback)</td>
    <td style="padding:15px">Returns address for a given address Id</td>
    <td style="padding:15px">{base_path}/{version}/address/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressDelete(collectionKey, addressId, callback)</td>
    <td style="padding:15px">Delete Address</td>
    <td style="padding:15px">{base_path}/{version}/address/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressExists(collectionKey, callback)</td>
    <td style="padding:15px">Determines if collection exists and contains addresses</td>
    <td style="padding:15px">{base_path}/{version}/address/exists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressUpdateAddress(collectionKey, addressUpdateModel, callback)</td>
    <td style="padding:15px">Update address in collection</td>
    <td style="padding:15px">{base_path}/{version}/address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressAddAddress(collectionKey, addressModel, callback)</td>
    <td style="padding:15px">Create new address in collection
Creates new collection if one does not exist</td>
    <td style="padding:15px">{base_path}/{version}/address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressCopyAddress(collectionKey, addressId, callback)</td>
    <td style="padding:15px">Copy existing address to new address</td>
    <td style="padding:15px">{base_path}/{version}/address/copy/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressClear(collectionKey, callback)</td>
    <td style="padding:15px">Delete all addresses in collection</td>
    <td style="padding:15px">{base_path}/{version}/address/clear/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressMoveAddress(sourceCollectionKey, destinationCollectionKey, addressId, callback)</td>
    <td style="padding:15px">Moves an address from one collection to another.</td>
    <td style="padding:15px">{base_path}/{version}/address/move/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressSearch(collectionKey, page, pageSize, name, stateProvince, postalCode, countryCode, searchText, useAndOperator, callback)</td>
    <td style="padding:15px">Search for addresses in collection</td>
    <td style="padding:15px">{base_path}/{version}/address/search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressGetAddressCountries(region, callback)</td>
    <td style="padding:15px">Get Countries codes and validation pattern for a given region</td>
    <td style="padding:15px">{base_path}/{version}/address/countries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressGetAddressProvinceStates(countryCode, callback)</td>
    <td style="padding:15px">Get provinces / state codes for given countryCode</td>
    <td style="padding:15px">{base_path}/{version}/address/provinces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressValidate(addressModel, callback)</td>
    <td style="padding:15px">Validate address</td>
    <td style="padding:15px">{base_path}/{version}/address/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartGetCart(cartCollectionKey, cartId, callback)</td>
    <td style="padding:15px">Returns current cart for the given cartCollectionKey
optional id returns cart specified in cart collection</td>
    <td style="padding:15px">{base_path}/{version}/cart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartUpdateCart(cartCollectionKey, cartModel, updateCartItems, callback)</td>
    <td style="padding:15px">Update cart in cart collection</td>
    <td style="padding:15px">{base_path}/{version}/cart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartAddCart(cartCollectionKey, cartModel, currentCart, callback)</td>
    <td style="padding:15px">Create new cart in cart collection, optionally set as current cart
Creates new cart collection if one does not exist</td>
    <td style="padding:15px">{base_path}/{version}/cart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartDelete(cartCollectionKey, cartId, callback)</td>
    <td style="padding:15px">Delete current cart or specific cart</td>
    <td style="padding:15px">{base_path}/{version}/cart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartExists(cartCollectionKey, callback)</td>
    <td style="padding:15px">Determines if cart collection exists and contains carts</td>
    <td style="padding:15px">{base_path}/{version}/cart/exists/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartCopyCart(cartCollectionKey, cartId, currentCart, callback)</td>
    <td style="padding:15px">Copy existing cart to new cart 
Optionally set new cart as current cart</td>
    <td style="padding:15px">{base_path}/{version}/cart/copy/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartGetCartList(cartCollectionKey, name, callback)</td>
    <td style="padding:15px">Returns an array of carts contained by the collection matching the received "cartCollectionKey".
This method can be used for client applications to display a list of carts for the user to select
as their current cart for systems which allow multiple carts per user.</td>
    <td style="padding:15px">{base_path}/{version}/cart/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartAddItem(cartCollectionKey, cartItemModel, cartId, callback)</td>
    <td style="padding:15px">Add cart item to current cart or specified cartId</td>
    <td style="padding:15px">{base_path}/{version}/cart/item/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartUpdateItem(cartCollectionKey, cartId, cartItemUpdateModel, callback)</td>
    <td style="padding:15px">Update cart item</td>
    <td style="padding:15px">{base_path}/{version}/cart/item/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartDeleteItem(cartCollectionKey, itemId, cartId, callback)</td>
    <td style="padding:15px">Delete current cart item or specific cart item</td>
    <td style="padding:15px">{base_path}/{version}/cart/item/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartClear(cartCollectionKey, callback)</td>
    <td style="padding:15px">Delete all carts in cart collection</td>
    <td style="padding:15px">{base_path}/{version}/cart/clear/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartHasCurrent(cartCollectionKey, callback)</td>
    <td style="padding:15px">Indicates if cart collection has current cart</td>
    <td style="padding:15px">{base_path}/{version}/cart/hascurrent/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartSwitchCurrentCart(cartCollectionKey, cartId, callback)</td>
    <td style="padding:15px">For client applications which allow multiple carts per user, provides a way to switch what is
considered the user's current cart to the cart matching the given cartID.</td>
    <td style="padding:15px">{base_path}/{version}/cart/switch/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartMoveCart(sourceCollectionKey, destinationCollectionKey, cartId, callback)</td>
    <td style="padding:15px">Moves a cart from one collection to another.  This could be used by a caller for many
purposes such as providing a way to move a cart from one user to another, or to
provide the server-side implementation of an approval queue by creating a collectionKey
for the queue, and moving the cart to the queue collection upon submission.</td>
    <td style="padding:15px">{base_path}/{version}/cart/move/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartGetCheckoutInfo(cartCollectionKey, cartId, callback)</td>
    <td style="padding:15px">Get checkout info from current cart or specified cart by cartId</td>
    <td style="padding:15px">{base_path}/{version}/cart/checkoutinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cartSaveCheckoutInfo(cartCollectionKey, cartCheckoutInfo, cartId, callback)</td>
    <td style="padding:15px">Add or update checkout info to current cart or cart specified by cartId</td>
    <td style="padding:15px">{base_path}/{version}/cart/checkoutinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ciscoDartGetSummary(customerNumber, callback)</td>
    <td style="padding:15px">Cisco Dart summary</td>
    <td style="padding:15px">{base_path}/{version}/ciscodart/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ciscoDartGetDetail(dealId, callback)</td>
    <td style="padding:15px">Cisco Dart detail</td>
    <td style="padding:15px">{base_path}/{version}/ciscodart/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customConfigGetCustomConfigs(customerNumber, callback)</td>
    <td style="padding:15px">Get list of custom configurations</td>
    <td style="padding:15px">{base_path}/{version}/customconfig/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customerGetCustCreditData(customerNumber, region, callback)</td>
    <td style="padding:15px">Returns customer status, revenue, credit limit, for a given customer</td>
    <td style="padding:15px">{base_path}/{version}/customer/creditdata/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customerGetPayers(customerNumber, businessUnit, callback)</td>
    <td style="padding:15px">Returns list of payers for a given customer</td>
    <td style="padding:15px">{base_path}/{version}/customer/payers/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invoiceGetInvoiceSummary(customerNumber, page, pageSize, salesOrderNumber, invoiceNumber, poNumber, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get Invoice Summary allows customers to get a paged list of invoices.
Search parameters can be specified to search by serial numbers and mac addresses within a given date range</td>
    <td style="padding:15px">{base_path}/{version}/invoice/summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invoiceGetInvoiceList(customerNumber, page, pageSize, salesOrderNumber, invoiceNumber, poNumber, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get Invoice List allows customers to get a paged list of invoices with details.</td>
    <td style="padding:15px">{base_path}/{version}/invoice/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invoiceGetInvoiceDetail(customerNumber, invoiceNumber, excludeSerialTracking, callback)</td>
    <td style="padding:15px">Invoice Detail - returns details about an invoice including serial and tracking numbers</td>
    <td style="padding:15px">{base_path}/{version}/invoice/detail/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">invoiceGetPDF(customerNumber, invoiceNumber, callback)</td>
    <td style="padding:15px">Get PDF of invoice</td>
    <td style="padding:15px">{base_path}/{version}/invoice/pdf/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partnerProfileGetPartnerProfile(aPIKey, developerEmail, callback)</td>
    <td style="padding:15px">Get partners profile</td>
    <td style="padding:15px">{base_path}/{version}/partnerprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partnerProfileGetValidate(developerEmail, customerNumber, url, callback)</td>
    <td style="padding:15px">Determines if partner profile is valid for specific api / resource</td>
    <td style="padding:15px">{base_path}/{version}/partnerprofile/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productPricing(pricingRequest, callback)</td>
    <td style="padding:15px">Product pricing returns SPA adjusted customer pricing, MSRP, dealer authorized flag, available and on order quantities.
SPA information includes flag indicating pricing is SPA adjusted, SPA description, flag indicating there is a SPA restriction, SPA restriction description.
SPA restrictions can include expiration date, min/max quantities.</td>
    <td style="padding:15px">{base_path}/{version}/product/pricing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productAvailability(request, callback)</td>
    <td style="padding:15px">Product availability returns quantity available given a list of warehouse numbers and item numbers</td>
    <td style="padding:15px">{base_path}/{version}/product/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productSearch(customerNumber, itemNumber, partNumberType, manufacturer, catalogName, categoryPath, includeObsolete, searchText, useAndOperator, region, page, pageSize, callback)</td>
    <td style="padding:15px">Product search allows for search for products by item number, manufacturer, general search text against descriptions</td>
    <td style="padding:15px">{base_path}/{version}/product/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">productGetDetail(customerNumber, itemNumber, partNumberType, region, callback)</td>
    <td style="padding:15px">Get detail about a product</td>
    <td style="padding:15px">{base_path}/{version}/product/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rmaCreateRMA(request, callback)</td>
    <td style="padding:15px">Create RMA</td>
    <td style="padding:15px">{base_path}/{version}/rma?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderCreateOrder(po, callback)</td>
    <td style="padding:15px">Create a new sales order in ScanSource’s ERP system.
When the order is created clients receive a response with a sales order number.</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderCreateOrderAsync(po, callback)</td>
    <td style="padding:15px">Create a new sales order in ScanSource’s ERP system.
When the order is created clients receive a response with sales order number.</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/createAsync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetVRD(request, callback)</td>
    <td style="padding:15px">Allows consumer to know which vendor required fields are necessary to collect before order creation
fields returned should be included on the salesorder/create VRDAnswers array</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/vrd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetOrderSummary(customerNumber, page, pageSize, salesOrderNumber, poNumber, referenceNumber, status, docType, fromDate, toDate, fromLastChangeDate, toLastChangeDate, serialNumbers, macAddresses, callback)</td>
    <td style="padding:15px">Get Order Summary allows customers to get a paged list of sales orders.
Search parameters can be specified to search by serial numbers and mac addresses within a given date range</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesorderSummary(page, pageSize, salesOrderNumber, poNumber, referenceNumber, status, docType, fromDate, toDate, fromLastChangeDate, toLastChangeDate, serialNumbers, macAddresses, callback)</td>
    <td style="padding:15px">Provide a resource for internal applications to get all recently updated order summaries regardless of customer #</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetOrderList(customerNumber, page, pageSize, salesOrderNumber, poNumber, referenceNumber, status, docType, fromDate, toDate, fromLastChangeDate, toLastChangeDate, callback)</td>
    <td style="padding:15px">Get Order List allows customers to get a paged list of sales orders with details.</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetShipQuote(request, callback)</td>
    <td style="padding:15px">ShipQuote allows customers to get a list of available shipping options with freight amounts given a shipping address and list of items</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/shipquote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetOrderDetail(customerNumber, salesOrderNumber, excludeSerialTracking, callback)</td>
    <td style="padding:15px">Sales Order Detail - returns details about an order including serial and tracking numbers</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/detail/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetTrackingNumbers(customerNumber, orderNumber, poNumber, deliveryNumber, callback)</td>
    <td style="padding:15px">Sales Order tracking allows customers to get tracking numbers for a given sales order, po or delivery</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/tracking/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderCancel(customerNumber, salesOrderNumber, purchaseOrderNumber, callback)</td>
    <td style="padding:15px">Submit request to process order cancellation</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">salesOrderGetSerialNumbers(customerNumber, page, pageSize, salesOrderNumber, poNumber, invoiceNumber, serialNumbers, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get serial numbers in date range / search serial numbers</td>
    <td style="padding:15px">{base_path}/{version}/salesorder/serials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spaAddSPA(request, callback)</td>
    <td style="padding:15px">Create SPA agreement</td>
    <td style="padding:15px">{base_path}/{version}/spa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spaGetAgreementSummary(customerNumber, salesOrg, dealId, endUser, mfgCode, validOn, controlNumber, description, agreementTypes, callback)</td>
    <td style="padding:15px">SPA summary</td>
    <td style="padding:15px">{base_path}/{version}/spa/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spaGetAgreementDetail(request, callback)</td>
    <td style="padding:15px">Get agreement detail by agreement number</td>
    <td style="padding:15px">{base_path}/{version}/spa/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemCentralGetSummary(customerNumber, page, pageSize, logNumber, poNumber, status, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get Summary allows customers to get a paged list of System Central logs.
Search parameters can be specified to search by log number, po number, statuses within a given date range</td>
    <td style="padding:15px">{base_path}/{version}/systemcentral/summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingGetTracking(customerNumber, poNumber, salesOrderNumber, callback)</td>
    <td style="padding:15px">Get tracking info</td>
    <td style="padding:15px">{base_path}/{version}/tracking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingGetShipmentTracking(customerNumber, poNumber, salesOrderNumber, callback)</td>
    <td style="padding:15px">Get tracking info</td>
    <td style="padding:15px">{base_path}/{version}/tracking/shipment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vendorOrderGetVendorSubscription(subscriptionId, callback)</td>
    <td style="padding:15px">Get vendor usage by invoice</td>
    <td style="padding:15px">{base_path}/{version}/vendororder/subscription/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vendorQuoteGetVendorQuote(vendor, quoteNumber, callback)</td>
    <td style="padding:15px">Get vendor quote</td>
    <td style="padding:15px">{base_path}/{version}/vendorquote/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vendorQuoteGetVendorQuoteList(vendor, dealId, dealName, quoteName, status, fromDate, toDate, callback)</td>
    <td style="padding:15px">Get list of quotes</td>
    <td style="padding:15px">{base_path}/{version}/vendorquote/list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vendorUsageGetVendorUsage(vendor, bid, invoiceNumber, callback)</td>
    <td style="padding:15px">Get vendor usage by invoice</td>
    <td style="padding:15px">{base_path}/{version}/vendorusage/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webHookGetHooks(callback)</td>
    <td style="padding:15px">Returns list of webhooks currently subscribed to</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webHookSubscribe(webHookModel, callback)</td>
    <td style="padding:15px">Subscribe to webhook to start receiving event notifications</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webHookDelete(id, callback)</td>
    <td style="padding:15px">Delete a webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
