# Adapter for ScanSource

Vendor: ScanSource
Homepage: https://www.scansource.com/

Product: ScanSource
Product Page: https://www.scansource.com/

## Introduction
We classify ScanSource into the ITSM domain as ScanSource specializes in the distribution of barcode, point-of-sale, networking, security, and communications equipment.

## Why Integrate
The ScanSource adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ScanSource. With this adapter you have the ability to perform operations on items such as:

- Product
- Quote
- Order
- Invoice

## Additional Product Documentation
The [API documents for ScanSource](https://services.scansource.com//swagger/ui/index)